# `my_println` as a proc macro

This is a Cargo [Workspace][workspaces] that contains:
 * `my_println` proc-macro crate
 * `sample` binary crate

This is part of my exploration into Rust Macros. The full slide deck can be
found [here][hello-rust-macros].

Note: unfortunately by default Rust does not allow procedural macros to expand
to statements. There is an unstable feature flag `proc_macro_hygiene` that must
be enabled to use this macro. An alternative approach to working around this
limitation is [proc-macro-hack][hack] by the brilliant David Tolnay. Check out
[my-println-proc-macro-hack][println-proc-macro-hack].

see: sample/src/main.rs (requires nightly)

```rs
#![feature(proc_macro_hygiene)]

use my_println::my_println;

fn main() {
    my_println!();
    my_println!("Hello, world!");
    my_println!("Hello, {}!", "PDX Rust");
}
```

If a user of this macro forgets to add `#![feature(proc_macro_hygiene)]` in
their code, then they'll get the error:

```
error[E0658]: procedural macros cannot be expanded to statements
```

[workspaces]: https://doc.rust-lang.org/book/ch14-03-cargo-workspaces.html
[hello-rust-macros]: https://gitlab.com/rcousineau/hello-rust-macros
[hack]: https://github.com/dtolnay/proc-macro-hack
[println-proc-macro-hack]: https://gitlab.com/rcousineau/my-println-proc-macro-hack
